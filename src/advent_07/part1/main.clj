(ns advent-07.part1.main)

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn parse-intcodes
  [s]
  (->> (clojure.string/split s #",")
       (map parse-int)))

(defn ->intcode-state-machine
  ([memory] (->intcode-state-machine memory 0))
  ([memory instruction-counter]
   { :memory memory
    :instruction-counter instruction-counter
    :state :running
    :input-buffer []
    :output-buffer [] }))

(defn intcode-at
  [{memory :memory} index]
  (memory index))

(defn- intcode-at-offset
  [{instruction-counter :instruction-counter :as state-machine} offset]
  (let [offset-intcode-index (+ instruction-counter offset)]
    (intcode-at state-machine offset-intcode-index)))

(defn- next-intcode
  [state-machine]
  (intcode-at-offset state-machine 0))

(defn update-intcode-at
  [state-machine index value]
  (assoc-in state-machine [:memory index] value))

; Valid read flags are :param-flag :param-override-value :param-override-deref
(defn- read-params
  [state-machine & read-flags]
  (let [intcode (next-intcode state-machine)
        param-flags (quot intcode 100) ; Removes the rightmost two digits representing the opcode, leaving the flags
        param-value #(intcode-at-offset state-machine %)
        param-deref #(intcode-at state-machine (param-value %))]
    (loop [[current-read-flag & remaining-read-flags] read-flags
           param-flags param-flags
           param-offset 1
           processed-params []]
      (let [current-param (case current-read-flag
                            :param-override-value (param-value param-offset)
                            :param-override-deref (param-deref param-offset)
                            :param-flag (if (= (int (mod param-flags 10)) 0)
                                          (param-deref param-offset)
                                          (param-value param-offset)))
            processed-params (conj processed-params current-param)]
        (if (empty? remaining-read-flags)
          processed-params
          (recur remaining-read-flags (quot param-flags 10) (+ param-offset 1) processed-params))))))

(defn- set-instruction-counter
  [state-machine target-index]
  (assoc state-machine :instruction-counter target-index))

(defn- advance-instruction-counter
  [state-machine instruction-count]
  (update-in state-machine [:instruction-counter] + instruction-count))

(defn- halt-and-catch-fire
  [state-machine]
  (assoc state-machine :state :halted))

(defn buffer-input
  [state-machine x]
  (update-in state-machine [:input-buffer] conj x))

(defn buffer-output
  [state-machine x]
  (update-in state-machine [:output-buffer] conj x))

(defn read-input
  [state-machine]
  { :input (first (state-machine :input-buffer))
    :state-machine (update-in state-machine [:input-buffer] rest) })

(defn read-output
  [state-machine]
  { :output (first (state-machine :output-buffer))
    :state-machine (update-in state-machine [:output-buffer] rest) })

; The step multimethod chooses which implementation to call based on the next opcode in memory
(defmulti step #(mod (next-intcode %) 100))

(defn safe-step
  [state-machine]
  (let [stepped (step state-machine)]
    stepped))

(defn run
  [state-machine]
  (if (= (:state state-machine) :running)
    (recur (safe-step state-machine))
    state-machine))

; A number of the instructions in the instruction-set are slight variations of one another,
; such as JMP if zero and JMP if not zero. Rather than reimplement each instruction, I'm choosing
; to couple together the similar instructions with the understanding that, should the instructions
; be independently changed, the some instructions might need to be reimplemented. The similar logic
; has been pulled into a series of "instruction-base-DESCRIPTION-f" functions that take only a
; state-machine and the fn representing the part of the behavior that is not common to all.

(defn- instruction-base-math-f-zero
  [state-machine f]
  (let [[x y target-index] (read-params state-machine :param-flag :param-flag :param-override-value)
        result (f x y)]
    (-> state-machine
        (update-intcode-at target-index result)
        (advance-instruction-counter 4))))

; ADD instruction - "1 x y target-index"
(defmethod step 1
  [state-machine]
  (instruction-base-math-f-zero state-machine +))

; MUL instruction - "2 x y target-index"
(defmethod step 2
  [state-machine]
  (instruction-base-math-f-zero state-machine *))

; INPUT instruction - "3 target-index"
(defmethod step 3
  [state-machine]
  (let [[target-index] (read-params state-machine :param-override-value)
        {input-value :input updated-state-machine :state-machine} (read-input state-machine)]
    (-> updated-state-machine
        (update-intcode-at target-index input-value)
        (advance-instruction-counter 2))))

; OUTPUT instruction - "4 target-index"
(defmethod step 4
  [state-machine]
  (let [[output-value] (read-params state-machine :param-flag)]
    (-> state-machine
        (buffer-output output-value)
        (advance-instruction-counter 2))))

(defn- instruction-base-jmp-f-zero
  [state-machine f]
  (let [[test-value target-index] (read-params state-machine :param-flag :param-flag)]
    (if (f test-value 0)
      (set-instruction-counter state-machine target-index)
      (advance-instruction-counter state-machine 3))))

; JNZ instruction - "5 test jmp-index"
(defmethod step 5
  [state-machine]
  (instruction-base-jmp-f-zero state-machine not=))

; JZ instruction - "6 test jmp-index"
(defmethod step 6
  [state-machine]
  (instruction-base-jmp-f-zero state-machine =))

(defn- instruction-base-cmp-f
  [state-machine f]
  (let [[x y target-index] (read-params state-machine :param-flag :param-flag :param-override-value)]
    (-> state-machine
        (update-intcode-at target-index (if (f x y) 1 0))
        (advance-instruction-counter 4))))

; Less Than instruction - "7 x y target-index"
(defmethod step 7
  [state-machine]
  (instruction-base-cmp-f state-machine <))

; CMP - "8 x y target-index"
(defmethod step 8
  [state-machine]
  (instruction-base-cmp-f state-machine =))

; HCL instruction - "99"
(defmethod step 99
  [state-machine]
  (halt-and-catch-fire state-machine))


;;; Main

; This definitely isn't a scalable solution, but for the size
; of values we're working with, it is scalable *enough*.
(def potential-circuit-orders
  (let [smallest-potential-value 1234
        largest-potential-value 43210
        all-values (->> (range smallest-potential-value (+ largest-potential-value 1))
                        (map #(format "%05d" %))
                        (map #(clojure.string/split % #""))
                        (map #(map parse-int %)))
        required-values (set [0 1 2 3 4])]
    (filter #(= (set %)
                required-values)
            all-values)))

(def intcode-memory-filename "inputs/07.txt")

(defn run-test
  [state-machine sequence]
  (let [all-amplifiers (map #(buffer-input state-machine %) sequence)]
    (reduce (fn [iter amplifier]
              (let [primed-stage (buffer-input amplifier iter)
                    completed-stage (run primed-stage)
                    output (:output (read-output completed-stage))]
                 output)) 0 all-amplifiers)))

; Outputs 298586
(defn run-part!
  []
  (let [initial-state-machine (-> intcode-memory-filename
                     slurp
                     parse-intcodes
                     vec
                     ->intcode-state-machine)
        ordered-circuit-orders (->> potential-circuit-orders
                                    (map #(run-test initial-state-machine %))
                                    (apply max))]
    (println "Day 7 - Part 1:" ordered-circuit-orders)))
