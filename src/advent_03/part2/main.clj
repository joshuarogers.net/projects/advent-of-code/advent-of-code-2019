(ns advent-03.part2.main
  (:require [advent-03.part1.parser :as parser]
            [clojure.set :as sets]))

(def filename "inputs/03.txt")

(defn distance
  [lines point]
  (->> lines
       (map vec)
       (map #(.indexOf % point))
       (apply +')))

; Outputs 721
(defn run-part!
  []
  (let [lines (->> filename
                   slurp
                   clojure.string/split-lines
                   (map parser/parse-command-text)
                   (map parser/rasterize-turtle-commands))
        common-points (apply sets/intersection (map set lines))
        common-point-distances (map #(distance lines %) common-points)
        nearest-common-point-distances (sort common-point-distances)]
    (println "Day 3 - Part 2:" (second nearest-common-point-distances))))
