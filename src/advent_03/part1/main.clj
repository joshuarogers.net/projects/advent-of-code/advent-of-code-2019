(ns advent-03.part1.main
  (:require [advent-03.part1.parser :as parser]
            [clojure.set :as sets]))

(def filename "inputs/03.txt")

(defn abs
  [x]
  (Math/abs x))

(defn distance
  [{x :x y :y}]
  (+ (abs x)
     (abs y)))

; Outputs 721
(defn run-part!
  []
  (let [lines (->> filename
                   slurp
                   clojure.string/split-lines
                   (map parser/parse-command-text)
                   (map parser/rasterize-turtle-commands))
        common-points (apply sets/intersection (map set lines))
        common-point-distances (map distance common-points)
        nearest-common-point-distances (sort common-point-distances)]
    (println "Day 3 - Part 1:" (second nearest-common-point-distances))))
