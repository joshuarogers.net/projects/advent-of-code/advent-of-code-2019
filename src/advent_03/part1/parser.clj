(ns advent-03.part1.parser)

(defn parse-int
  [x]
  (Integer/parseInt x))

(defn ->point
  [x y]
  {:x x
   :y y})

(defn- ->turtle-command
  [direction distance]
  {:direction direction
   :distance distance})

(defn rasterize-turtle-command
  [{direction :direction distance :distance} {x :x y :y}]
  (let [range-degrees (map #(+ 1 %)
                           (range distance))
        relative-point-fn (case direction
                            "U" #(->point 0 %)
                            "D" #(->point 0 (- 0 %))
                            "L" #(->point % 0)
                            "R" #(->point (- 0 %) 0))
        relative-points (map relative-point-fn range-degrees)]
    (map (fn [{relative-x :x relative-y :y}]
           (->point (+ relative-x x)
                    (+ relative-y y)))
         relative-points)))

(defn rasterize-turtle-commands
  [commands]
  (loop [points [(->point 0 0)]
         commands commands]
    (if (empty? commands)
      points
      (let [[command & unprocessed-commands] commands
            last-point (last points)
            new-points (rasterize-turtle-command command last-point)
            all-points (concat points new-points)]
        (recur all-points unprocessed-commands)))))

(defn parse-command-text
  [s]
  (->> s
       (re-seq #"([A-Z])([0-9]+)")
       (map (fn [[_ direction distance]]
             (->turtle-command direction (parse-int distance))))))